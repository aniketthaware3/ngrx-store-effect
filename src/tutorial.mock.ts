
import { Tutorial } from './app/store/models/tutorial.model';

export const TUTORIALS: Tutorial[] = [
    {
        "name": "ngRx",
        "url": "https://ngrx.io/docs"
    },
    {
        "name": "Angular",
        "url": "https://angular.io/docs"
    },
    {
        "name": "Angular Material",
        "url": "https://material.angular.io/"
    }
];