import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Tutorial} from '../store/models/tutorial.model';
import {Store} from '@ngrx/store';
import {AppState} from '../store/reducers/tutorial.state';
import * as TutorialActions from './../store/actions/tutorial.actions'
@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.css']
})
export class ReadComponent implements OnInit {
  tutorials: Observable<Tutorial[]>;

  constructor(private store: Store<AppState>) {
    this.tutorials = store.select('tutorial')
    this.store.dispatch(new TutorialActions.LoadTutorial())
  }

  ngOnInit() {
  }

  deleteTutorial(index){
    let arr = [{name:"Aniet",url:"rerer"},{name:"vaibbhav",url:"rerer"}]
    arr.splice(0,1);
    console.log("hiii"+JSON.stringify(arr));
    this.store.dispatch(new TutorialActions.RemoveTutorial(index));
  }

}
