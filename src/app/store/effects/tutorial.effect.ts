import { Injectable } from "@angular/core";
import { Effect,Actions, ofType, createEffect } from '@ngrx/effects';
import * as tutorialActions from "./../actions/tutorial.actions"
import { TutorialService } from '../service/tutorial.service';
import { map, mergeMap, catchError, tap, switchMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
@Injectable()
export class TutorialEffects {
    constructor(private actions$:Actions,private tutorialService:TutorialService){}

   @Effect()
   loadTutorial$ = this.actions$.pipe(
    ofType(tutorialActions.LOAD_TUTORIAL),
    mergeMap((action) => this.tutorialService.getAllTutorial().pipe(
    map( tutorials => {
        return (new tutorialActions.LoadTutorialSuccess(tutorials))
    })
    ))
  );
}