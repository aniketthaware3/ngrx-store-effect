import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Tutorial } from '../models/tutorial.model';
import { TUTORIALS } from 'src/tutorial.mock';
 @Injectable({
  providedIn: 'root'
})
export class TutorialService {

  constructor(private http:HttpClient) { }

  public getAllTutorial():Observable<any>{
      return of(TUTORIALS);
  }

}
